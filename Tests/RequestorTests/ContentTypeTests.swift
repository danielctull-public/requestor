
@testable import Requestor
import XCTest

final class URLRequestContentTypeTests: XCTestCase {

    func testEmptyContentType() {
        let string = ""
        let contentType = ContentType(string)
        XCTAssertEqual(contentType.mediaType, .form)
        XCTAssertEqual(contentType.encoding, .utf8)
    }

	func testContentTypeForm() {
		let string = "application/x-www-form-urlencoded"
		let contentType = ContentType(string)
		XCTAssertEqual(contentType.mediaType, .form)
		XCTAssertEqual(contentType.encoding, .utf8)
	}

	func testContentTypeFormCharsetUTF16() {
		let string = "application/x-www-form-urlencoded; charset=utf-16"
		let contentType = ContentType(string)
		XCTAssertEqual(contentType.mediaType, .form)
		XCTAssertEqual(contentType.encoding, .utf16)
	}

	func testContentTypeJSON() {
		let string = "application/json"
		let contentType = ContentType(string)
		XCTAssertEqual(contentType.mediaType, .json)
		XCTAssertEqual(contentType.encoding, .utf8)
	}

	func testContentTypeJSONCharsetUTF16() {
		let string = "application/json; charset=utf-16"
		let contentType = ContentType(string)
		XCTAssertEqual(contentType.mediaType, .json)
		XCTAssertEqual(contentType.encoding, .utf16)
	}
}
