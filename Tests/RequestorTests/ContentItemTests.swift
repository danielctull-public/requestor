
import Requestor
import XCTest

final class ContentItemTests: XCTestCase {

    func testGetItemsForm() {
        let string = "key1=value1&key2=value2"
        let type = ContentType(mediaType: .form, encoding: .utf8)
        let data = string.data(using: type.encoding)!
        let content = Content(contentType: type, data: data)
        let items = content.items
        XCTAssertEqual(items.count, 2)
        XCTAssert(items.contains(Content.Item(key: "key1", value: "value1")))
        XCTAssert(items.contains(Content.Item(key: "key2", value: "value2")))
    }

    func testGetItemsFormOnlyKey() {
        let string = "key1"
        let type = ContentType(mediaType: .form, encoding: .utf8)
        let data = string.data(using: type.encoding)!
        let content = Content(contentType: type, data: data)
        let items = content.items
        XCTAssertEqual(items.count, 1)
        XCTAssert(items.contains(Content.Item(key: "key1", value: "")))
    }

    func testGetItemsBadForm() {
        let string = " "
        let type = ContentType(mediaType: .form, encoding: .utf8)
        let data = string.data(using: type.encoding)!
        let content = Content(contentType: type, data: data)
        let items = content.items
        XCTAssertEqual(items.count, 0)
    }

    func testInitItemsForm() {
        let items = [
            Content.Item(key: "key1", value: "value1"),
            Content.Item(key: "key2", value: "value2")
        ]
        let type = ContentType(mediaType: .form, encoding: .utf8)
        let content = Content(contentType: type, items: items)
        let data = "key1=value1&key2=value2".data(using: .utf8)
        XCTAssertEqual(content?.data, data)
    }

    func testGetItemsJSON() {
        let string = """
            {"key1":"value1","key2":"value2"}
            """
        let type = ContentType(mediaType: .json, encoding: .utf8)
        let data = string.data(using: type.encoding)!
        let content = Content(contentType: type, data: data)
        let items = content.items
        XCTAssertEqual(items.count, 2)
        XCTAssert(items.contains(Content.Item(key: "key1", value: "value1")))
        XCTAssert(items.contains(Content.Item(key: "key2", value: "value2")))
    }

    func testGetItemsBadJSON() {
        let string = "_"
        let type = ContentType(mediaType: .json, encoding: .utf8)
        let data = string.data(using: type.encoding)!
        let content = Content(contentType: type, data: data)
        let items = content.items
        XCTAssertEqual(items.count, 0)
    }

    func testInitItemsJSON() {
        let items = [
            Content.Item(key: "key1", value: "value1"),
            Content.Item(key: "key2", value: "value2")
        ]
        let type = ContentType(mediaType: .json, encoding: .utf8)
        let content = Content(contentType: type, items: items)
        let data = """
            {"key1":"value1","key2":"value2"}
            """.data(using: .utf8)
        XCTAssertEqual(content?.data, data)
    }
}
