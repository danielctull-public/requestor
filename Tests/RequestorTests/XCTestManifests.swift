#if !canImport(ObjectiveC)
import XCTest

extension StringEncodingCharsetTests {
    // DO NOT MODIFY: This is autogenerated, use:
    //   `swift test --generate-linuxmain`
    // to regenerate.
    static let __allTests__StringEncodingCharsetTests = [
        ("testCharsetASCII", testCharsetASCII),
        ("testCharsetUTF16", testCharsetUTF16),
        ("testCharsetUTF32", testCharsetUTF32),
        ("testCharsetUTF8", testCharsetUTF8),
        ("testInitLowercaseASCII", testInitLowercaseASCII),
        ("testInitLowercaseUTF16", testInitLowercaseUTF16),
        ("testInitLowercaseUTF32", testInitLowercaseUTF32),
        ("testInitLowercaseUTF8", testInitLowercaseUTF8),
        ("testInitUppercaseASCII", testInitUppercaseASCII),
        ("testInitUppercaseUTF16", testInitUppercaseUTF16),
        ("testInitUppercaseUTF32", testInitUppercaseUTF32),
        ("testInitUppercaseUTF8", testInitUppercaseUTF8),
    ]
}

extension URLRequestContentTypeTests {
    // DO NOT MODIFY: This is autogenerated, use:
    //   `swift test --generate-linuxmain`
    // to regenerate.
    static let __allTests__URLRequestContentTypeTests = [
        ("testContentTypeForm", testContentTypeForm),
        ("testContentTypeFormCharsetUTF16", testContentTypeFormCharsetUTF16),
        ("testContentTypeJSON", testContentTypeJSON),
        ("testContentTypeJSONCharsetUTF16", testContentTypeJSONCharsetUTF16),
    ]
}

public func __allTests() -> [XCTestCaseEntry] {
    return [
        testCase(StringEncodingCharsetTests.__allTests__StringEncodingCharsetTests),
        testCase(URLRequestContentTypeTests.__allTests__URLRequestContentTypeTests),
    ]
}
#endif
