
@testable import Requestor
import XCTest

final class StringEncodingCharsetTests: XCTestCase {

	func testInitUppercaseASCII() {
		let encoding = String.Encoding(charset: "ASCII")
		XCTAssertEqual(encoding, .ascii)
	}

	func testInitLowercaseASCII() {
		let encoding = String.Encoding(charset: "ascii")
		XCTAssertEqual(encoding, .ascii)
	}

	func testCharsetASCII() {
		XCTAssertEqual(String.Encoding.ascii.charset, "us-ascii")
	}

	func testInitUppercaseUTF8() {
		let encoding = String.Encoding(charset: "UTF-8")
		XCTAssertEqual(encoding, .utf8)
	}

	func testInitLowercaseUTF8() {
		let encoding = String.Encoding(charset: "utf-8")
		XCTAssertEqual(encoding, .utf8)
	}

	func testCharsetUTF8() {
		XCTAssertEqual(String.Encoding.utf8.charset, "utf-8")
	}

	func testInitUppercaseUTF16() {
		let encoding = String.Encoding(charset: "UTF-16")
		XCTAssertEqual(encoding, .utf16)
	}

	func testInitLowercaseUTF16() {
		let encoding = String.Encoding(charset: "utf-16")
		XCTAssertEqual(encoding, .utf16)
	}

	func testCharsetUTF16() {
		XCTAssertEqual(String.Encoding.utf16.charset, "utf-16")
	}

	func testInitUppercaseUTF32() {
		let encoding = String.Encoding(charset: "UTF-32")
		XCTAssertEqual(encoding, .utf32)
	}

	func testInitLowercaseUTF32() {
		let encoding = String.Encoding(charset: "utf-32")
		XCTAssertEqual(encoding, .utf32)
	}

	func testCharsetUTF32() {
		XCTAssertEqual(String.Encoding.utf32.charset, "utf-32")
	}

    func testEmptyCharset() {
        let encoding = String.Encoding(charset: "")
        XCTAssertNil(encoding)
    }
}
