import XCTest

import RequestorTests

var tests = [XCTestCaseEntry]()
tests += RequestorTests.__allTests()

XCTMain(tests)
