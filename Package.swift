// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "Requestor",
    platforms: [
        .macOS(.v10_13)
    ],
    products: [
        .library(
            name: "Requestor",
            targets: ["Requestor"]),
    ],
    targets: [
        .target(
            name: "Requestor"),
        .testTarget(
            name: "RequestorTests",
            dependencies: ["Requestor"]),
    ]
)
