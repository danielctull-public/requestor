FROM swift:latest

WORKDIR /package

COPY . ./

RUN swift --version
CMD swift test