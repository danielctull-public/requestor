
import CoreFoundation

extension String.Encoding {

	init?(charset: String) {

		let cfencoding = CFStringConvertIANACharSetNameToEncoding(charset as CFString)

		guard cfencoding != kCFStringEncodingInvalidId else {
			return nil
		}

		let nsencoding = CFStringConvertEncodingToNSStringEncoding(cfencoding)
		self = String.Encoding(rawValue: nsencoding)
	}

	var charset: String {
		let cfencoding = CFStringConvertNSStringEncodingToEncoding(rawValue)
		let cfcharset = CFStringConvertEncodingToIANACharSetName(cfencoding)!
		return cfcharset as String
	}
}
