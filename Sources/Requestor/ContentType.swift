
import Foundation

public struct ContentType {

    public let mediaType: MediaType
    public let encoding: String.Encoding

    public init(mediaType: MediaType, encoding: String.Encoding) {
        self.mediaType = mediaType
        self.encoding = encoding
    }

    init(_ string: String) {

        let parameters = "mime=".appending(string)
            .components(separatedBy: ";")
            .reduce([:]) { result, string -> [String: String] in

                let components = string.components(separatedBy: "=")
                guard components.count > 1 else {
                    return result
                }

                let key = components[0].trimmingCharacters(in: .whitespaces)
                let value = components[1].trimmingCharacters(in: .whitespaces)

                var dictionary = result
                dictionary[key] = value
                return dictionary
        }

        if let mime = parameters["mime"], let mediaType = MediaType(rawValue: mime) {
            self.mediaType = mediaType
        } else {
            self.mediaType = .form
        }

        if let charset = parameters["charset"], let encoding = String.Encoding(charset: charset) {
            self.encoding = encoding
        } else {
            self.encoding = .utf8
        }
    }
}

extension ContentType {

	var headerValue: String {
		return "\(mediaType); charset=\(encoding.charset)"
	}

	static let headerField = "Content-Type"
}
