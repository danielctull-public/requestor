
import Foundation

public struct Content {

    public let data: Data
    public let contentType: ContentType

    public init(contentType: ContentType, data: Data) {
        self.contentType = contentType
        self.data = data
    }
}

extension URLRequest {

	public var content: Content? {
		get {

			guard
				let contentType = allHTTPHeaderFields?[ContentType.headerField].map(ContentType.init),
				let httpBody = httpBody
			else {
				return nil
			}

			return Content(contentType: contentType, data: httpBody)
		}
		set {
			setValue(newValue?.contentType.headerValue, forHTTPHeaderField: ContentType.headerField)
			httpBody = newValue?.data
		}
	}
}
