
import Foundation

extension Content {

    public struct Item: Equatable {
        public let key: String
        public let value: String

        public init(key: String, value: String) {
            self.key = key
            self.value = value
        }
    }
}

extension Content {

    public init?(contentType: ContentType, items: [Item]) {

        switch contentType.mediaType {
        case .form:
            var components = URLComponents()
            components.queryItems = items.map(URLQueryItem.init)
            guard let string = components.percentEncodedQuery else { return nil }
            guard let data = string.data(using: contentType.encoding) else { return nil }
            self.init(contentType: contentType, data: data)

        case .json:
            let dictionary = Dictionary(uniqueKeysWithValues: items.map { ($0.key, $0.value) })
            guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .sortedKeys) else { return nil }
            self.init(contentType: contentType, data: data)
        }
    }

    public var items: [Item] {
        get {
            switch contentType.mediaType {
            case .form:
                let string = String(data: data, encoding: contentType.encoding)
                guard string?.rangeOfCharacter(from: CharacterSet.urlQueryAllowed.inverted) == nil else { return [] }
                var components = URLComponents()
                components.percentEncodedQuery = string
                let items = components.queryItems ?? []
                return items.map(Item.init)
            case .json:
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let dictionary = json as? [String: String]
                return dictionary?.map(Item.init) ?? []
            }
        }
    }
}

extension URLQueryItem {
    fileprivate init(_ item: Content.Item) {
        self.init(name: item.key, value: item.value)
    }
}

extension Content.Item {
    fileprivate init(_ queryItem: URLQueryItem) {
        self.init(key: queryItem.name, value: queryItem.value ?? "")
    }
}
