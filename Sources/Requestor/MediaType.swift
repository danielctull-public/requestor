
import Foundation

public enum MediaType: String {
    case form = "application/x-www-form-urlencoded"
    case json = "application/json"
}
